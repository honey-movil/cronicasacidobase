package mx.unam.fciencias.pdm.cronicasacidobase;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // note that we are NOT using setContentView
        // instead, the Splash screen is defined by the style
        Intent intent = new Intent(this, MainActivity.class);
        // The MainActivity may perform heavy loads in onCreate
        // The splash is here to let the user know the app is slowly launching
        // or just showing off some fancy logo
        startActivity(intent);
        finish();
    }

}
