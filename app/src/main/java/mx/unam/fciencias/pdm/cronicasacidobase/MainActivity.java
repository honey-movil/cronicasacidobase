package mx.unam.fciencias.pdm.cronicasacidobase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<DataClass> dataList;
    MyAdapter adapter;
    DataClass androidData;
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        searchView = findViewById(R.id.search);

        searchView.clearFocus();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchList(newText);
                return true;
            }
        });

        GridLayoutManager gridLayoutManager = new GridLayoutManager(MainActivity.this, 1);
        recyclerView.setLayoutManager(gridLayoutManager);
        dataList = new ArrayList<>();

        androidData = new DataClass("3000 A.c",R.string.SeisMil, "3000 A.c", R.drawable.tres_mil);
        dataList.add(androidData);

        androidData = new DataClass("6000 A.c",R.string.SeisMil, "6000 A.c", R.drawable.seis_mil);
        dataList.add(androidData);

        androidData = new DataClass("1400",R.string.SeisMil, "1400", R.drawable.mil_cuatro);
        dataList.add(androidData);

        androidData = new DataClass("1600",R.string.SeisMil, "1600", R.drawable.mil_seis);
        dataList.add(androidData);

        androidData = new DataClass("1700",R.string.SeisMil, "1700", R.drawable.mil_siete);
        dataList.add(androidData);

        androidData = new DataClass("1800",R.string.SeisMil, "1800", R.drawable.mil_ocho);
        dataList.add(androidData);

        androidData = new DataClass("1800",R.string.SeisMil, "1900", R.drawable.mil_nueve);
        dataList.add(androidData);

        adapter = new MyAdapter(MainActivity.this,dataList);
        recyclerView.setAdapter(adapter);
    }

    private  void  searchList(String text){
        List<DataClass> dataSearchList = new ArrayList<>();
        for (DataClass data: dataList) {
            if (data.getDataTitle().toLowerCase().contains(text.toLowerCase())) {
                dataSearchList.add(data);
            }
        }
        if(dataSearchList.isEmpty()){
            Toast.makeText(this, "Not Found", Toast.LENGTH_SHORT).show();
        } else {
            adapter.setSearchList(dataSearchList);
        }

        // Aquí buscamos al personaje en este caso es: "La cpher"
        if (text.equalsIgnoreCase("La cpher")) {
            // Iniciar la actividad nanocarruselActivity2
            Intent intent = new Intent(MainActivity.this, nocarruselActivity2.class);
            startActivity(intent);
        }
    }
}
