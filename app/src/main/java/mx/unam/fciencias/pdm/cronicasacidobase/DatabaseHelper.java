package mx.unam.fciencias.pdm.cronicasacidobase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    //nombre de la bd.
    private static final String DATABASE_NAME = "characters.db";
    // versión de la bd.
    private static final int DATABASE_VERSION = 1;
    //nombre de la tabla de la bd.
    public static final String TABLE_NAME = "characters";
    // id del personaje
    public static final String COLUMN_ID = "_id";
    // nombre del personaje
    public static final String COLUMN_TITLE = "title";
    // época del personaje.
    public static final String COLUMN_EPOCH = "epoch";

    // creamos la tabla
    private static final String CREATE_TABLE_QUERY = "CREATE TABLE " + TABLE_NAME + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_TITLE + " TEXT, " +
            COLUMN_EPOCH + " INTEGER)";
    // agregamos los campos a la bd.
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    //creación.
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_QUERY);
    }
    // actualización.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    // Método para agregar un nuevo personaje
    public void addCharacter(String title, int epoch) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, title);
        values.put(COLUMN_EPOCH, epoch);
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    // Método para eliminar un personaje por su nombre
    public void deleteCharacterByTitle(String title) {
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = COLUMN_TITLE + " = ?";
        String[] selectionArgs = {title};
        db.delete(TABLE_NAME, selection, selectionArgs);
        db.close();
    }

    // Método para consultar un personaje por su nombre
    public Cursor getCharacterByTitle(String title) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {COLUMN_ID, COLUMN_TITLE, COLUMN_EPOCH};
        String selection = COLUMN_TITLE + " = ?";
        String[] selectionArgs = {title};
        return db.query(TABLE_NAME, projection, selection, selectionArgs, null, null, null);
    }
}


